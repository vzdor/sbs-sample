require 'rails_helper'

RSpec.describe SubscribersController, :type => :controller do
  render_views

  describe '#index' do
    before do
      connection = stub_peytzmail
      response = double 'response', body: read_fixture('subscribers.json')
      expect(connection).to receive(:get) { response }

    end

    it 'lists malinglist subscribers' do
      get :index, mailinglist_id: 1
      expect(response).to be_success
    end
  end

  describe '#create' do
    it 'adds a new subscriber' do
      connection = stub_peytzmail
      attrs = {email: 'bob@test.com', first_name: 'Bob', last_name: 'Z'}
      payload = {subscribe: {
                   mailinglist_ids: ["1"],
                   subscriber: attrs.as_json,
                 }}
      expect(connection)
        .to receive(:post)
             .with('mailinglists/subscribe.json', payload)
      post :create, mailinglist_id: 1, subscriber: attrs
      expect(response).to be_redirect
    end
  end

  describe '#new' do
    it 'renders form' do
      get :new, mailinglist_id: 1
      expect(response).to be_success
    end
  end
end
