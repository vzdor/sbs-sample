require 'rails_helper'

RSpec.describe MailinglistsController, :type => :controller do
  describe "#index" do
    render_views

    before do
      connection = stub_peytzmail
      response = double 'response', body: read_fixture('mailinglists.json')
      expect(connection).to receive(:get) { response }
    end

    it 'renders successfully' do
      get :index
      expect(response).to be_success
    end
  end
end
