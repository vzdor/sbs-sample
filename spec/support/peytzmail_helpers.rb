module PeytzmailHelpers
  def read_fixture(file)
    f = File.join(Rails.root, 'spec/fixtures', file)
    File.read(f)
  end

  def stub_peytzmail
    connection = double 'connection'
    expect(Peytzmail::Client::Connection).to receive(:new) { connection }
    connection
  end
end
