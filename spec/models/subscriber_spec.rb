require 'rails_helper'

RSpec.describe Subscriber do
  describe '::url_root' do
    it 'adds malinglists prefix' do
      opts = {mailinglist_id: 1}
      expect(Subscriber.url_root(opts)).to eq("mailinglists/1")
    end
  end
end
