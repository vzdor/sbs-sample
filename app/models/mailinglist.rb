class Mailinglist
  # requered attributes
  attr_accessor :title, :default_template, :send_confirmation_mail, :send_welcome_mail

  # optional
  attr_accessor :description

  attr_accessor :id

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include Peytzmail::Client

  # FIXME: ActiveModel::Conversion returns the self obj. So...
  def to_param
    id
  end
end
