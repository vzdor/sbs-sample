module Peytzmail
  module Client
    extend ActiveSupport::Concern

    class Connection < Faraday::Connection
      def initialize(options = {})
        defaults = {url: 'https://test.peytzmail.com/api/v1'}
        super(defaults.merge(options))
        adapter(:net_http)
        response(:logger)
        # So this is an authenticated connection
        basic_auth($peytzmail_key, '')
      end
    end

    included do
      extend(ClassMethods)
    end

    def save
      if id.present?
        put
      else
        run_callbacks :create do
          post
        end
      end
    end

    def post
      self.class
        .post(as_json)
    end

    module ClassMethods
      def url_root(options = {}, method = :get)
        ''
      end

      # This will be user to compose the url. Also, to get
      #  the reponse body data.
      # So for Mailinglist, it will return "mailinglists".
      # You can overwrite this in the model.
      def url_name(method = :get)
        to_s
          .downcase
          .pluralize
      end

      # Combines url_root and url_name
      # For example: /mailinglists/:id/subscribers
      def full_url(options = {}, method = :get)
        url = [url_root(options, method), url_name(method) + '.json']
              .reject(&:blank?)
              .join('/')
        url
      end

      def get_one(id, options = {})

      end

      def instantiate_one(attrs)
        # TODO: We do not declare all fields. So we can't do this:
        #   new(attrs)
        # will raise UknownAttr.. error. will need somethign safer. Also, how the fuck they can trust an unknown API server?
        obj = new
        attrs.each do |k, v|
          obj.public_send("#{k}=", v)
        end rescue nil
        obj
      end

      def get_many(options = {})
        # TODO: Add debugging
        con = Connection.new
        response = con.get(full_url(options))

        data = JSON.load(response.body)

        ary = data[url_name]
        ary.map { |attrs| instantiate_one(attrs) }
      end

      def post(options = {})
        con = Connection.new
        con.post full_url(options, :post), options
      end

      def all(options = {})
        get_many(options)
      end
    end
  end
end
