# /api/v1/mailinglists/<mailinglist-id>/subscribers.json
class Subscriber
  attr_accessor :first_name, :last_name, :mailinglist_id

  # required
  attr_accessor :email, :malinglist_ids

  attr_accessor :id

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include Peytzmail::Client

  extend ActiveModel::Callbacks

  define_model_callbacks :create

  before_create :align_mailinglist_ids

  # FIXME:
  def to_param
    id
  end

  # We want to move malinglist_id into malinglist_ids
  def align_mailinglist_ids
    @mailinglist_ids ||= []
    @mailinglist_ids << @mailinglist_id
    @mailinglist_ids.uniq!
  end

  # I could not find an example of subscribing with /subscribers api.
  # It seems /subscribe just adds a new subscriber,
  #  so we use /mailinglists/:id/subscribe
  def subscribe
    run_callbacks :create do
      con = Connection.new
      url = [Mailinglist.url_name, 'subscribe'].join('/') + '.json'
      payload = subscribe_payload

      response = con.post(url, payload)

      Rails.logger.debug response.body
      Rails.logger.debug payload
    end
  end

  def subscribe_payload
    only = %w(email first_name last_name)
    payload = {
      subscribe:
        {
          mailinglist_ids: @mailinglist_ids,
          subscriber: as_json(only: only)
        }
    }
  end

  # def self.url_name(method = :get)
  #   if method == :post
  #     'subscribe' # XXX: ugly.
  #   else
  #     super
  #   end
  # end

  def self.url_root(options = {}, method = :get)
    # If we want filtering by mailinglist_id
    if method == :get && options.key?(:mailinglist_id)
      ['mailinglists', options[:mailinglist_id]]
        .flatten
        .join('/')
    else
      super
    end
  end
end
