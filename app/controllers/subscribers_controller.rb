class SubscribersController < ApplicationController
  def index
    @subscribers = Subscriber.all(mailinglist_options)
  end

  def create
    subscriber = Subscriber.new(subscriber_params)
    subscriber.subscribe
    redirect_to mailinglist_subscribers_path(mailinglist_options)
  end

  def new
    @subscriber = Subscriber.new
  end

  private

  def mailinglist_options
    {mailinglist_id: params[:mailinglist_id]}
  end

  def subscriber_params
    attrs = params
            .require(:subscriber)
            .permit(:email, :first_name, :last_name)
    attrs.merge(mailinglist_options)
  end
end
